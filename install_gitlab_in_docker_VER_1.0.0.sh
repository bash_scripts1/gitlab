#!/bin/bash

# узнаем IP вм на которой будет выполнятся установка gitlab
export GITLAB_IP=$(hostname -I | awk '{print $1}')


#------------------------------------------------- body of function -----------------------------------------------------


# utilits
yum update -y
yum install -y epel-release
yum install -y jq
yum install -y mc nmap chrony wget nano psmisc zip unzip  yum-utils net-tools nfs-utils tree htop


#======== Установка Gitlab с помощью Docker ===================

# Установка Docker
# В данном разделе мы рассмотрим пример установки Docker на Centos 7 используя Colvir репозиторий.

# log file
if touch gitlab_installation_logs.txt
then
rm gitlab_installation_logs.txt
touch gitlab_installation_logs.txt
else
touch gitlab_installation_logs.txt
fi

# date
DATE=$(date)
sleep 2
echo "------------------------------------------------------------" >> /root/gitlab_installation_logs.txt
echo "-------------------- ${DATE} -------------------------------" >> /root/gitlab_installation_logs.txt
echo "------------------------------------------------------------" >> /root/gitlab_installation_logs.txt

# docker
if systemctl enable docker
then
echo "Docker уже установлен"
DOCKER_VER=$(docker version)
echo "------------- Версия docker -------------" >> /root/gitlab_installation_logs.txt
echo "${DOCKER_VER}" >> /root/gitlab_installation_logs.txt
systemctl status docker
sleep 2
else
echo "Установка Docker"
yum install -y yum-utils
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum -y install docker-ce docker-ce-cli containerd.io
systemctl enable docker --now
curl -SL https://github.com/docker/compose/releases/download/v2.6.1/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
echo "Docker установлен"
DOCKER_VER=$(docker version)
echo "------------- Версия docker -------------" >> /root/gitlab_installation_logs.txt
echo "${DOCKER_VER}" >> /root/gitlab_installation_logs.txt
DOCKER_COMPOSE_VER=$(docker-compose --version)
echo "------------- Версия docker-compose -------------" >> /root/gitlab_installation_logs.txt
echo "${DOCKER_COMPOSE_VER}" >> /root/gitlab_installation_logs.txt
systemctl status docker

sleep 2
fi

# gitlab
if docker images | grep gitlab
then
echo "Gitlab уже установлен"
echo "Gitlab уже установлен" >> /root/gitlab_installation_logs.txt
sleep 2

else
echo "Устанавливаем gitlab"
sleep 2

echo "Устанавливаем gitlab" >> /root/gitlab_installation_logs.txt

echo "Создаем и заходим в каталог gitlab"
sleep 2

echo "Создаем и заходим в каталог gitlab" >> /root/gitlab_installation_logs.txt

mkdir gitlab && cd gitlab


echo "Редактируем файл docker-compose-gitlab.yml"
sleep 2

echo "Редактируем файл docker-compose-gitlab.yml" >> /root/gitlab_installation_logs.txt

touch docker-compose-gitlab.yml
 

echo "Вставляем код в файл docker-compose-gitlab.yml"
sleep 2

cat <<EOS > /root/gitlab/docker-compose-gitlab.yml
version: '3.6'
services:
  web:
    image: 'gitlab/gitlab-ee:latest'
    restart: always
    container_name: gitlab
    hostname: 'gitlab.${GITLAB_IP}'
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'http://gitlab.${GITLAB_IP}'
        registry_external_url 'http://registry-gitlab.${GITLAB_IP}'
        # Add any other gitlab.rb configuration here, each on its own line
    ports:
      - '80:80'
      - '443:443'
     #- '22:22'
    volumes:
      - '/root/gitlab/config:/etc/gitlab'
      - '/root/gitlab/logs:/var/log/gitlab'
      - '/root/gitlab/data:/var/opt/gitlab'
    shm_size: '256m'
EOS

echo "------------- Файл docker-compose-gitlab.yml --------------------" >> /root/gitlab_installation_logs.txt
DOC_COM_GIT=$(cat /root/gitlab/docker-compose-gitlab.yml)
echo "${DOC_COM_GIT}" >> /root/gitlab_installation_logs.txt
echo "-----------------------------------------------------------------" >> /root/gitlab_installation_logs.txt

cat /root/gitlab/docker-compose-gitlab.yml
sleep 5

echo "Устанавливаем непосредственно gitlab"
sleep 2

echo "------------- Устанавливаем непосредственно gitlab -----------------------" >> /root/gitlab_installation_logs.txt
GIT_INSTALL_FN=$(docker-compose -f /root/gitlab/docker-compose-gitlab.yml up -d)
echo "${GIT_INSTALL_FN}" >> /root/gitlab_installation_logs.txt
echo "--------------------------------------------------------------------------" >> /root/gitlab_installation_logs.txt

fi


function check {
if docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
then
echo "------------- Логин и пароль для Gitlab -------------" >> /root/gitlab_installation_logs.txt
echo "login: root"  >> /root/gitlab_installation_logs.txt
echo "$(docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password)"  >> /root/gitlab_installation_logs.txt
echo "-----------------------------------------------------" >> /root/gitlab_installation_logs.txt
echo "Первый запуск Gitlab может занять до 20 минут"
echo "Первый запуск Gitlab может занять до 20 минут" >> /root/gitlab_installation_logs.txt
echo "Gitlab будет доступен по ссылке: http://gitlab.${GITLAB_IP}"
echo "Gitlab будет доступен по ссылке: http://gitlab.${GITLAB_IP}" >> /root/gitlab_installation_logs.txt
else
echo "Docker container gitlab создается, подождите... "
sleep 10
check
fi
}

check

echo "Сейчас будем наблюдать за загрузкой gitlab"
echo "Для выхода с режима обзора нажмите ctrl + c"
sleep 5

docker logs -f gitlab



# Если нужно внести изменения в файл - gitlab/docker-compose-gitlab.yml:

# Открываем файл и производим изменения, после чего нужно произвести переконфигурирование, что-бы изменения вступили в селу.

# Для этого:

# а) Удаляем контейнер:

# docker-compose -f /root/gitlab/docker-compose-gitlab.yml down
 
# б) Создаем контейнер:

# docker-compose -f /root/gitlab/docker-compose-gitlab.yml up -d
 
# в) Проверяем работоспособность c браузера:
